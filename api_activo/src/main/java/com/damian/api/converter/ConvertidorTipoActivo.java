package com.damian.api.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.damian.api.entity.TipoActivo;
import com.damian.api.model.MTipoActivo;

@Component("convertidorTipoActivo")
public class ConvertidorTipoActivo {
	

public List<MTipoActivo> convertirLista(List<TipoActivo> tipoActivos){
	
		List<MTipoActivo> mtipoActivos  = new ArrayList<>();
			
			for (TipoActivo tipoActivo: tipoActivos ) {
				
				mtipoActivos.add(new MTipoActivo(tipoActivo));
				
			} 
			
	return mtipoActivos;
}
	

}

package com.damian.api.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import com.damian.api.entity.EstadoActivo;
import com.damian.api.model.MEstadoActivo;



@Component("convertidorEstadoActivo")
public class ConvertidorEstadoActivo {
	
	
	public List<MEstadoActivo> convertirLista(List<EstadoActivo> estadoActivos){
		
		List<MEstadoActivo> mestadoActivos  = new ArrayList<>();
			
			for (EstadoActivo estadoActivo: estadoActivos ) {
				
				mestadoActivos.add(new MEstadoActivo(estadoActivo));
				
			} 
			
	return mestadoActivos;
}
	
	
}

package com.damian.api.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.damian.api.entity.Ciudad;
import com.damian.api.model.MCiudad;

@Component("convertidorCiudad")
public class ConvertidorCiudad {
	
	public List<MCiudad> convertirLista(List<Ciudad> ciudades){
		
		List<MCiudad> mciudades  = new ArrayList<>();
			
			for (Ciudad ciudad: ciudades ) {
				
				mciudades.add(new MCiudad(ciudad));
				
			} 
			
	return mciudades;
}

}

package com.damian.api.converter;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
import com.damian.api.entity.Activo;
import com.damian.api.model.MActivo;


@Component("convertidorActivo")
public class ConvertidorActivo {
	
public List<MActivo> convertirLista(List<Activo> activos){
		
		List<MActivo> mactivos  = new ArrayList<>();
			
			for (Activo activo: activos ) {
				
				mactivos.add(new MActivo(activo));
				
			} 
			
	return mactivos;
}
	
	
}

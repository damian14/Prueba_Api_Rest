package com.damian.api.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import com.damian.api.entity.AreaActivo;
import com.damian.api.model.MAreaActivo;

@Component("convertidorAreaActivo")
public class ConvertidorAreaActivo {
	
public List<MAreaActivo> convertirLista(List<AreaActivo> areaActivos){
		
		List<MAreaActivo> mareaActivos  = new ArrayList<>();
			
			for (AreaActivo areaActivo: areaActivos ) {
				
				mareaActivos.add(new MAreaActivo(areaActivo));
				
			} 
			
	return mareaActivos;
}

}

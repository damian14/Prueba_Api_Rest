package com.damian.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.damian.api.converter.ConvertidorEstadoActivo;
import com.damian.api.model.MEstadoActivo;
import com.damian.api.repository.EstadoActivoRepository;

@Service("servicioEstadoActivo")
public class EstadoActivoService {

	@Autowired
	@Qualifier("repositoryEstadoActivity")
	private EstadoActivoRepository repositorio;

	@Autowired
	@Qualifier("convertidorEstadoActivo")
	private ConvertidorEstadoActivo convertidor;

	public List<MEstadoActivo> obtenerEstadosActivos(Pageable pageable) {

		return convertidor.convertirLista(repositorio.findAll(pageable).getContent());
	}

	public MEstadoActivo obtnerPorId(Long idEstadoActivo) {

		try {
			MEstadoActivo mEstadoActivo;
			return mEstadoActivo = new MEstadoActivo(repositorio.findByIdEstadoActivo(idEstadoActivo));
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}

	}

}

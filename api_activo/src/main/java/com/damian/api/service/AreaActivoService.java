package com.damian.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.damian.api.converter.ConvertidorAreaActivo;
import com.damian.api.entity.AreaActivo;
import com.damian.api.entity.Ciudad;
import com.damian.api.model.MAreaActivo;
import com.damian.api.repository.AreaActivoRepository;
import com.damian.api.repository.CiudadRepository;

@Service("servicioAreaActivo")
public class AreaActivoService {

	@Autowired
	@Qualifier("repositoryAreaActivo")
	private AreaActivoRepository repositorio;

	@Autowired
	@Qualifier("repositoryCiudad")
	private CiudadRepository ciudadRepository;

	@Autowired
	@Qualifier("convertidorAreaActivo")
	private ConvertidorAreaActivo convertidor;

	public List<MAreaActivo> obtenerAreaActivos(Pageable pageable) {

		List<AreaActivo> listAreaActivo = new ArrayList<>();
		try {
			listAreaActivo = repositorio.findAll(pageable).getContent();
			for (int i = 0; i < listAreaActivo.size(); i++) {
				Long idCiudad;
				idCiudad = listAreaActivo.get(i).getIdCiudad().getIdCiudad();
				Ciudad ciudad;
				ciudad = obtenerCiudad(idCiudad);
				listAreaActivo.get(i).setIdCiudad(ciudad);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return convertidor.convertirLista(listAreaActivo);
	}

	public MAreaActivo obtenerPorId(Long idAreaActivo) {

		try {
			MAreaActivo mAreaActivo;
			Ciudad ciudad;
			AreaActivo areaActivo = new AreaActivo();
			areaActivo = repositorio.findByIdAreaActivo(idAreaActivo);
			Long idCiudad = areaActivo.getIdCiudad().getIdCiudad();
			ciudad = obtenerCiudad(idCiudad);
			areaActivo.setIdCiudad(ciudad);
			return mAreaActivo = new MAreaActivo(areaActivo);
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}

	}

	public String guardarAreaActivo(AreaActivo areaActivo) {
		String respuesta="";
		try {
			repositorio.save(areaActivo);	
			respuesta="Se guardo bien";
		} catch (Exception e) {
			// TODO: handle exception
			respuesta=e.getMessage();
		}
		return respuesta;

	}

	public Ciudad obtenerCiudad(Long idCiudad) {
		Ciudad ciudad;
		ciudad = new Ciudad(ciudadRepository.findByIdCiudad(idCiudad));
		return ciudad;
	}

}

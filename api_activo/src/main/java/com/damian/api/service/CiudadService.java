package com.damian.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.damian.api.converter.ConvertidorCiudad;
import com.damian.api.model.MCiudad;
import com.damian.api.repository.CiudadRepository;

@Service("servicioCiudad")
public class CiudadService {
	
	@Autowired
	@Qualifier("repositoryCiudad")
	private CiudadRepository repositorio;

	@Autowired
	@Qualifier("convertidorCiudad")
	private ConvertidorCiudad convertidor;

	public List<MCiudad> obtenerCiudades(Pageable pageable) {

		return convertidor.convertirLista(repositorio.findAll(pageable).getContent());
	}
	
	public MCiudad obtenerPorId(Long idCiudad) {
		try {
			MCiudad mCiudad;
			return mCiudad =  new MCiudad(repositorio.findByIdCiudad(idCiudad));
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
		
	}
	
}

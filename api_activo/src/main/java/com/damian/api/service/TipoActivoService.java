package com.damian.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.damian.api.converter.ConvertidorTipoActivo;
import com.damian.api.model.MTipoActivo;
import com.damian.api.repository.TipoActivoRepository;

@Service("servicioTipoActivo")
public class TipoActivoService {

	@Autowired
	@Qualifier("repositoryTipoActivo")
	private TipoActivoRepository repositorio;

	@Autowired
	@Qualifier("convertidorTipoActivo")
	private ConvertidorTipoActivo convertidor;

	public List<MTipoActivo> obtenertTipodeActivos(Pageable pageable) {

		return convertidor.convertirLista(repositorio.findAll(pageable).getContent());
	}

	public MTipoActivo obtenerTipoActivoId(Long idTipoActivo) {

		try {
			MTipoActivo mTipoActivo;
			return mTipoActivo = new MTipoActivo(repositorio.findByIdTipoActivo(idTipoActivo));
			// return null;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}

	}
}

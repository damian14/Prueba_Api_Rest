package com.damian.api.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.damian.api.converter.ConvertidorActivo;
import com.damian.api.entity.Activo;
import com.damian.api.entity.AreaActivo;
import com.damian.api.entity.Ciudad;
import com.damian.api.entity.EstadoActivo;
import com.damian.api.entity.TipoActivo;
import com.damian.api.model.MActivo;
import com.damian.api.repository.ActivoRepository;
import com.damian.api.repository.AreaActivoRepository;
import com.damian.api.repository.CiudadRepository;
import com.damian.api.repository.EstadoActivoRepository;
import com.damian.api.repository.TipoActivoRepository;

@Service("servicioActivo")
public class ActivoService {

	@Autowired
	@Qualifier("repositoryActivo")
	private ActivoRepository activoRepositorio;

	@Autowired
	@Qualifier("repositoryAreaActivo")
	private AreaActivoRepository areaActivoRepository;

	@Autowired
	@Qualifier("repositoryCiudad")
	private CiudadRepository ciudadRepository;

	@Autowired
	@Qualifier("repositoryEstadoActivity")
	private EstadoActivoRepository estadoActivoRepository;

	@Autowired
	@Qualifier("repositoryTipoActivo")
	private TipoActivoRepository tipoActivoRepository;

	@Autowired
	@Qualifier("convertidorActivo")
	private ConvertidorActivo convertidorActivo;

	public boolean crear(Activo activo) {
		// logger.info("Creando Nota");

		try {

			activoRepositorio.save(activo);
			// logger.info("Nota Creada");
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			// logger.error("Se genero un error al crear la nota");
			return false;
		}
	}

	public List<MActivo> obtenerActivos(Pageable pageable) {
		// logger.info("Creando Nota");
		List<Activo> listaActivo = new ArrayList<>();
		try {

			listaActivo = activoRepositorio.findAll();
			listaActivo = ObtenerListaActivos(listaActivo);
			if(listaActivo.isEmpty()) {
				return null;
			}else {
				// logger.info("Nota Creada");
				return convertidorActivo.convertirLista(listaActivo);
			}
		} catch (Exception e) {
			// TODO: handle exception
			// logger.error("Se genero un error al crear la nota");
			return null;
		}
	}

	public List<MActivo> obtenerActivosPorFiltros(Long idTipoActivo, String fechaCompra, String serial) {
		List<Activo> listaActivo = new ArrayList<>();
		Date fechaCompra1=convertirFecha(fechaCompra);
		try {
			listaActivo = activoRepositorio.findByIdTipoActivoIdTipoActivoAndFechaCompraAndSerial(idTipoActivo,
					fechaCompra1, serial);
			listaActivo = ObtenerListaActivos(listaActivo);
			// logger.info("Nota Creada");
			if(listaActivo.isEmpty()) {
				return null;
			}else {
				
				return convertidorActivo.convertirLista(listaActivo);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			// logger.error("Se genero un error al crear la nota");
			return null;
		}
	}

	public String guardarActivo(Activo activo) {
		String respuesta = "";
		try {
			activoRepositorio.save(activo);
		} catch (Exception e) {
			respuesta = e.getMessage();
			// TODO: handle exception
		}

		return respuesta;
	}

	public String actualizarActivo(Activo activo) {
		String respuesta = validarActivo(activo);
		if (respuesta.equals("")) {
			try {
				activoRepositorio.actualizarActivo(activo.getIdActivo(), activo.getSerial(), activo.getFechaBaja());
			} catch (Exception e) {
				// TODO: handle exception
				respuesta = e.getMessage();
			}
		}
		return respuesta;
	}

	public Ciudad obtenerCiudad(Long idCiudad) {
		Ciudad ciudad;
		ciudad = new Ciudad(ciudadRepository.findByIdCiudad(idCiudad));
		return ciudad;
	}

	public EstadoActivo obtenerEstadoActivo(Long idEstadoActivo) {
		EstadoActivo estadoActivo;
		estadoActivo = new EstadoActivo(estadoActivoRepository.findByIdEstadoActivo(idEstadoActivo));
		return estadoActivo;
	}

	public TipoActivo obtenerTipoActivo(Long idTipoActivo) {
		TipoActivo tipoActivo;
		tipoActivo = new TipoActivo(tipoActivoRepository.findByIdTipoActivo(idTipoActivo));
		return tipoActivo;
	}

	public AreaActivo obtenerAreaActivo(Long idAreaActivo) {
		AreaActivo areaActivo;
		areaActivo = new AreaActivo(areaActivoRepository.findByIdAreaActivo(idAreaActivo));
		return areaActivo;
	}

	public List<Activo> ObtenerListaActivos(List<Activo> listaActivos) {
		Long idCiudad, idTipoActivo, idEstadoActivo, idAreaActivo;
		AreaActivo areaActivo = new AreaActivo();
		TipoActivo tipoActivo = new TipoActivo();
		EstadoActivo estadoActivo = new EstadoActivo();
		Ciudad ciudad = new Ciudad();
		for (int i = 0; i < listaActivos.size(); i++) {
			idTipoActivo = listaActivos.get(i).getIdTipoActivo().getIdTipoActivo();
			idEstadoActivo = listaActivos.get(i).getIdEstadoActivo().getIdEstadoActivo();
			if (listaActivos.get(i).getIdAreaActivo() != null) {
				idAreaActivo = listaActivos.get(i).getIdAreaActivo().getIdAreaActivo();
				areaActivo = obtenerAreaActivo(idAreaActivo);
				idCiudad = areaActivo.getIdCiudad().getIdCiudad();
				ciudad = obtenerCiudad(idCiudad);
				areaActivo.setIdCiudad(ciudad);
			}
			estadoActivo = obtenerEstadoActivo(idEstadoActivo);
			tipoActivo = obtenerTipoActivo(idTipoActivo);
			listaActivos.get(i).setIdAreaActivo(areaActivo);
			listaActivos.get(i).setIdEstadoActivo(estadoActivo);
			listaActivos.get(i).setIdTipoActivo(tipoActivo);

		}

		return listaActivos;
	}

	public Date convertirFecha(String fechaConvertir) {
		DateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
		Date fechaConvertida = null;
		try {
			fechaConvertida = formatoDelTexto.parse(fechaConvertir);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return fechaConvertida;
	}

	public String validarActivo(Activo activo) {
		String respuesta = "";
		Activo activoConsulta;
		activoConsulta = activoRepositorio.findByIdActivo(activo.getIdActivo());
		if (activoConsulta != null) {
			activoConsulta = activoRepositorio.findBySerial(activo.getSerial());
			if (activo.getFechaBaja().before(activo.getFechaCompra())) {
				return respuesta = "La fecha de Ingreso no puede ser superior a la fecha de Baja";
			} else {
				if (activoConsulta == null) {
					return respuesta;
				} else if (activo.getIdActivo() == activoConsulta.getIdActivo()) {
					return respuesta;
				} else {
					return respuesta = "No puede existir dos activos con el mismo número de serial";
				}

			}
		} else {
			return respuesta = "El activo no existe";
		}

	}

}

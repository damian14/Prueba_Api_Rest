package com.damian.api.controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.damian.api.entity.Activo;
import com.damian.api.entity.AreaActivo;
import com.damian.api.model.MActivo;
import com.damian.api.model.MAreaActivo;
import com.damian.api.model.MCiudad;
import com.damian.api.model.MEstadoActivo;
import com.damian.api.model.MTipoActivo;
import com.damian.api.service.ActivoService;
import com.damian.api.service.AreaActivoService;
import com.damian.api.service.CiudadService;
import com.damian.api.service.EstadoActivoService;
import com.damian.api.service.TipoActivoService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@ControllerAdvice
@RequestMapping("/v1")
public class ActivoController {

	@Autowired
	@Qualifier("servicioActivo")
	private ActivoService servicio;

	@Autowired
	@Qualifier("servicioTipoActivo")
	private TipoActivoService tipoActivoService;

	@Autowired
	@Qualifier("servicioEstadoActivo")
	private EstadoActivoService estadoActivoService;

	@Autowired
	@Qualifier("servicioAreaActivo")
	private AreaActivoService areaActivoService;

	@Autowired
	@Qualifier("servicioCiudad")
	private CiudadService ciudadService;
	
	@ApiOperation(value = "Listar todas los tipos de activos")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 400, message = "Búsqueda sin resultados"), })
	@GetMapping("/tipoactivo")
	public List<MTipoActivo> obtenerTipoActivos(Pageable pageable) {

		return tipoActivoService.obtenertTipodeActivos(pageable);

	}

	@ApiOperation(value = "Listar todas los tipos de activos por idTipoActivo")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 400, message = "Búsqueda sin resultados"), })
	@GetMapping("/tipoactivo/{idTipoActivo}/")
	public MTipoActivo obtenerTipoActivoId(@PathVariable("idTipoActivo") Long idTipoActivo) {

		return tipoActivoService.obtenerTipoActivoId(idTipoActivo);

	}
	
	
	@ApiOperation(value = "Listar todas los estados")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 400, message = "Búsqueda sin resultados"), })
	@GetMapping("/estadoactivo")
	public List<MEstadoActivo> obtenerEstadoActivos(Pageable pageable) {

		return estadoActivoService.obtenerEstadosActivos(pageable);

	}
	
	@ApiOperation(value = "Listar todas los estados por idEstadoActivo")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 400, message = "Búsqueda sin resultados"), })
	@GetMapping("/estadoactivo/{idEstadoActivo}/")
	public MEstadoActivo obtenerTipoIdEstadoActivo(@PathVariable("idEstadoActivo") Long IdEstadoActivo) {

		return estadoActivoService.obtnerPorId(IdEstadoActivo);

	}
	
	@ApiOperation(value = "Listar todas las areas")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 400, message = "Búsqueda sin resultados"), })
	@GetMapping("/areaactivo")
	public List<MAreaActivo> obtenerAreaActivos(Pageable pageable) {

		return areaActivoService.obtenerAreaActivos(pageable);

	}
	
	@ApiOperation(value = "Listar area por idArea")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 400, message = "Búsqueda sin resultados"), })
	@GetMapping("/areaactivo/{idAreaActivo}/")
	public MAreaActivo obtenerAreaActivoId(@PathVariable("idAreaActivo") Long idAreaActivo) {

		return areaActivoService.obtenerPorId(idAreaActivo);

	}
	
	
	@ApiOperation(value = "Listar la ciudades")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 400, message = "Búsqueda sin resultados"), })
	@GetMapping("/ciudad")
	public List<MCiudad> obtenerCiudades(Pageable pageable) {

		return ciudadService.obtenerCiudades(pageable);

	}
	

	@ApiOperation(value = "Listar la ciudades por idCiudad")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 400, message = "Búsqueda sin resultados"), })
	@GetMapping("/ciudad/{idCiudad}/")
	public MCiudad obtenerCiudadId(@PathVariable("idCiudad") Long idCiudad) {

		return ciudadService.obtenerPorId(idCiudad);

	}

	@ApiOperation(value = "Guardar Activo")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Se guardo satisfactoriamente el activo"),
							@ApiResponse(code = 400, message = "Error de datos faltantes"), })
	@PutMapping("/activo")
	public void agregarActivo(@RequestBody @Valid Activo activo, HttpServletResponse response) throws IOException {
		String respuesta = "";
		respuesta = servicio.guardarActivo(activo);
		if (respuesta.equals("")) {
			String mensaje = "Se guardo satisfactoriamente el activo";
			respuestaStatus(response, HttpStatus.OK, mensaje);
		} else {
			respuestaStatus(response, HttpStatus.BAD_REQUEST, respuesta);
		}

	}
	

	@ApiOperation(value = "Agregar area ")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "El area  se guardo realizó satisfactoriamente"),
			@ApiResponse(code = 400, message = "Error de actualización"), })
	@PutMapping("/areaactivo")
	public String agregarAreaActivo(@RequestBody @Valid AreaActivo areaActivo) {
		return areaActivoService.guardarAreaActivo(areaActivo);
	}

	@ApiOperation(value = "Actulizar Activo")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "La actualización se realizó satisfactoriamente"),
			@ApiResponse(code = 400, message = "La fecha de Ingreso no puede ser superior a la fecha de Baja / No puede existir dos activos con el mismo número de serial / El activo no existe"), })
	@PostMapping("/activo")
	public void actualizarActivo(@RequestBody @Valid Activo activo, HttpServletResponse response) throws IOException {
		String respuesta = "";
		respuesta = servicio.actualizarActivo(activo);
		if (respuesta.equals("")) {
			String mensaje = "La actualización se realizó satisfactoriamente";
			respuestaStatus(response, HttpStatus.OK, mensaje);
		} else {
			respuestaStatus(response, HttpStatus.BAD_REQUEST, respuesta);
		}

	}

	@ApiOperation(value = "Listar Todos los activos")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 404, message = "Búsqueda sin resultados"),
			@ApiResponse(code = 500, message = "Error de Backend ") })
	@GetMapping("/activo")
	public ResponseEntity obtenerActivos(Pageable pageable, HttpServletResponse response) {

		List<MActivo> listaActivos = new ArrayList<>();
		listaActivos = servicio.obtenerActivos(pageable);
		if (listaActivos == null) {
			String mensaje = "Búsqueda sin resultados";
			return new ResponseEntity<String>(mensaje, HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<MActivo>>(listaActivos, HttpStatus.OK);
		}

	}

	@ApiOperation(value = "Activo por idTipoActivo/fechaCompra/serial")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 404, message = "Búsqueda sin resultados "),
			@ApiResponse(code = 500, message = "Error de Backend ") })
	@GetMapping("/activo/{idTipoActivo}/{fechaCompra}/{serial}")
	public ResponseEntity obtenerActivos(@PathVariable("idTipoActivo") Long idTipoActivo,
			@PathVariable("fechaCompra") String fechaCompra, @PathVariable("serial") String serial,
			HttpServletResponse response) throws IOException {

		List<MActivo> listaActivos = new ArrayList<>();
		listaActivos = servicio.obtenerActivosPorFiltros(idTipoActivo, fechaCompra, serial);
		if (listaActivos == null) {
			String mensaje = "Búsqueda sin resultados";
			response.sendError(HttpStatus.BAD_REQUEST.value(), mensaje);
			return new ResponseEntity<String>(response.toString(),HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<List<MActivo>>(listaActivos, HttpStatus.OK);
		}

	}

	@ExceptionHandler(IllegalArgumentException.class)
	void respuestaStatus(HttpServletResponse response, HttpStatus ok, String mensaje) throws IOException {
		response.sendError(ok.value(), mensaje);
	}

	public List<MActivo> devolverLista(List<MActivo> listaActivos) {
		return listaActivos;
	}

}

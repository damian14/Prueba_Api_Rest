package com.damian.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class ApiActivoApplication {
	
	@Bean
	public Docket studentAPI() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.damian.api.controller"))
				.paths(PathSelectors.regex("/v1.*")).build()
				.apiInfo(metaData());
	}

	private ApiInfo metaData() {
		return new ApiInfoBuilder().title("Api Activos").description("Api Rest para el control de Activos")
				.contact(new Contact("Damian Arenales", null, "damianonepiece@gmail.com")).build();
	}

	public static void main(String[] args) {
		SpringApplication.run(ApiActivoApplication.class, args);
	}
}

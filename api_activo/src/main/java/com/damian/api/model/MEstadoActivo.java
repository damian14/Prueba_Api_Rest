package com.damian.api.model;


import com.damian.api.entity.EstadoActivo;


public class MEstadoActivo {
	
	private Long idEstadoActivo;
    
    private String nombreEstado;
    
    private String descripcionEstado;
   

	public Long getIdEstadoActivo() {
		return idEstadoActivo;
	}

	public void setIdEstadoActivo(Long idEstadoActivo) {
		this.idEstadoActivo = idEstadoActivo;
	}

	public String getNombreEstado() {
		return nombreEstado;
	}

	public void setNombreEstado(String nombreEstado) {
		this.nombreEstado = nombreEstado;
	}

	public String getDescripcionEstado() {
		return descripcionEstado;
	}

	public void setDescripcionEstado(String descripcionEstado) {
		this.descripcionEstado = descripcionEstado;
	}

	public MEstadoActivo(Long idEstadoActivo, String nombreEstado, String descripcionEstado) {
		super();
		this.idEstadoActivo = idEstadoActivo;
		this.nombreEstado = nombreEstado;
		this.descripcionEstado = descripcionEstado;
	}

	public MEstadoActivo(EstadoActivo estadoActivo) {
		
		
		this.idEstadoActivo=estadoActivo.getIdEstadoActivo();
	    
		this.nombreEstado=estadoActivo.getNombreEstado();
	    
		this.descripcionEstado=estadoActivo.getDescripcionEstado();
	}

    
    
}

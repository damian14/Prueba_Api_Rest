package com.damian.api.model;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;

import com.damian.api.entity.Activo;
import com.damian.api.entity.AreaActivo;
import com.damian.api.entity.EstadoActivo;
import com.damian.api.entity.TipoActivo;

public class MActivo {
	
	    private Long idActivo;
	    
	    private String nombre;
	   
	    private String descripcion;
	    
	    private String serial;
	    
	    private int numeroInternoInventario;
	   
	    private BigInteger peso;
	   
	    private BigInteger alto;
	
	    private BigInteger ancho;
	   
	    private BigInteger largo;
	   
	    private double valorCompra;
	    
	    private Date fechaCompra;
	   
	    private Date fechaBaja;
	  
	    private String color;
	    
	    private AreaActivo idAreaActivo;
	    
	    private EstadoActivo idEstadoActivo;
	    
	    private TipoActivo idTipoActivo;
	    
	    private String marca;
	    
	    private BigInteger cantidad;

		public Long getIdActivo() {
			return idActivo;
		}

		public void setIdActivo(Long idActivo) {
			this.idActivo = idActivo;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getDescripcion() {
			return descripcion;
		}

		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}

		public String getSerial() {
			return serial;
		}

		public void setSerial(String serial) {
			this.serial = serial;
		}

		public int getNumeroInternoInventario() {
			return numeroInternoInventario;
		}

		public void setNumeroInternoInventario(int numeroInternoInventario) {
			this.numeroInternoInventario = numeroInternoInventario;
		}

		public BigInteger getPeso() {
			return peso;
		}

		public void setPeso(BigInteger peso) {
			this.peso = peso;
		}

		public BigInteger getAlto() {
			return alto;
		}

		public void setAlto(BigInteger alto) {
			this.alto = alto;
		}

		public BigInteger getAncho() {
			return ancho;
		}

		public void setAncho(BigInteger ancho) {
			this.ancho = ancho;
		}

		public BigInteger getLargo() {
			return largo;
		}

		public void setLargo(BigInteger largo) {
			this.largo = largo;
		}

		public double getValorCompra() {
			return valorCompra;
		}

		public void setValorCompra(double valorCompra) {
			this.valorCompra = valorCompra;
		}

		public Date getFechaCompra() {
			return fechaCompra;
		}

		public void setFechaCompra(Date fechaCompra) {
			this.fechaCompra = fechaCompra;
		}

		public Date getFechaBaja() {
			return fechaBaja;
		}

		public void setFechaBaja(Date fechaBaja) {
			this.fechaBaja = fechaBaja;
		}

		public String getColor() {
			return color;
		}

		public void setColor(String color) {
			this.color = color;
		}

		public AreaActivo getIdAreaActivo() {
			return idAreaActivo;
		}

		public void setIdAreaActivo(AreaActivo idAreaActivo) {
			this.idAreaActivo = idAreaActivo;
		}

		public EstadoActivo getIdEstadoActivo() {
			return idEstadoActivo;
		}

		public void setIdEstadoActivo(EstadoActivo idEstadoActivo) {
			this.idEstadoActivo = idEstadoActivo;
		}

		public TipoActivo getIdTipoActivo() {
			return idTipoActivo;
		}

		public void setIdTipoActivo(TipoActivo idTipoActivo) {
			this.idTipoActivo = idTipoActivo;
		}

		public MActivo(Long idActivo, String nombre, String descripcion, String serial, int numeroInternoInventario,
				BigInteger peso, BigInteger alto, BigInteger ancho, BigInteger largo, double valorCompra,
				Date fechaCompra, Date fechaBaja, String color,String marca,BigInteger cantidad, AreaActivo idAreaActivo, EstadoActivo idEstadoActivo,
				TipoActivo idTipoActivo) {
			super();
			this.idActivo = idActivo;
			this.nombre = nombre;
			this.descripcion = descripcion;
			this.serial = serial;
			this.numeroInternoInventario = numeroInternoInventario;
			this.peso = peso;
			this.alto = alto;
			this.ancho = ancho;
			this.largo = largo;
			this.valorCompra = valorCompra;
			this.fechaCompra = fechaCompra;
			this.fechaBaja = fechaBaja;
			this.color = color;
			this.idAreaActivo = idAreaActivo;
			this.idEstadoActivo = idEstadoActivo;
			this.idTipoActivo = idTipoActivo;
			this.marca=marca;
			this.cantidad=cantidad;
		}

		public MActivo(Activo activo) {
			
			this.idActivo = activo.getIdActivo();
			this.nombre = activo.getNombre();
			this.descripcion = activo.getDescripcion();
			this.serial = activo.getSerial();
			this.numeroInternoInventario = activo.getNumeroInternoInventario();
			this.peso = activo.getPeso();
			this.alto = activo.getAlto();
			this.ancho = activo.getAncho();
			this.largo = activo.getLargo();
			this.valorCompra = activo.getValorCompra();
			this.fechaCompra = activo.getFechaCompra();
			this.fechaBaja = activo.getFechaBaja();
			this.color = activo.getColor();
			this.idAreaActivo = activo.getIdAreaActivo();
			this.idEstadoActivo = activo.getIdEstadoActivo();
			this.idTipoActivo = activo.getIdTipoActivo();
			this.marca=activo.getMarca();
			this.cantidad=activo.getCantidad();
		}
	    
	    

}

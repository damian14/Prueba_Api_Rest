package com.damian.api.model;


import com.damian.api.entity.AreaActivo;
import com.damian.api.entity.Ciudad;

public class MAreaActivo {
	
	  	private Long idAreaActivo;
	    private String nombreAreaActivo;
	    private String descripcionAreaActivo;
	    private Ciudad idCiudad;
		public Long getIdAreaActivo() {
			return idAreaActivo;
		}
		public void setIdAreaActivo(Long idAreaActivo) {
			this.idAreaActivo = idAreaActivo;
		}
		public String getNombreAreaActivo() {
			return nombreAreaActivo;
		}
		public void setNombreAreaActivo(String nombreAreaActivo) {
			this.nombreAreaActivo = nombreAreaActivo;
		}
		public String getDescripcionAreaActivo() {
			return descripcionAreaActivo;
		}
		public void setDescripcionAreaActivo(String descripcionAreaActivo) {
			this.descripcionAreaActivo = descripcionAreaActivo;
		}
		public Ciudad getIdCiudad() {
			return idCiudad;
		}
		public void setIdCiudad(Ciudad idCiudad) {
			this.idCiudad = idCiudad;
		}
		
		
		
		public MAreaActivo(Long idAreaActivo, String nombreAreaActivo, String descripcionAreaActivo, Ciudad idCiudad) {
			super();
			this.idAreaActivo = idAreaActivo;
			this.nombreAreaActivo = nombreAreaActivo;
			this.descripcionAreaActivo = descripcionAreaActivo;
			this.idCiudad = idCiudad;
		}
		
		public MAreaActivo(AreaActivo areaActivo) {
			
			this.idAreaActivo = areaActivo.getIdAreaActivo();
			this.nombreAreaActivo = areaActivo.getNombreAreaActivo();
			this.descripcionAreaActivo = areaActivo.getDescripcionAreaActivo();
			this.idCiudad = areaActivo.getIdCiudad();
		}
	    
	    
	    

}

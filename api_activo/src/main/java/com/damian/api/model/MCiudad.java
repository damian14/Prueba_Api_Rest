package com.damian.api.model;

import com.damian.api.entity.Ciudad;

public class MCiudad {
	
	private Long idCiudad;
    private String nombreCiudad;
    private String descripcion;
    
    
	public Long getIdCiudad() {
		return idCiudad;
	}
	public void setIdCiudad(Long idCiudad) {
		this.idCiudad = idCiudad;
	}
	public String getNombreCiudad() {
		return nombreCiudad;
	}
	public void setNombreCiudad(String nombreCiudad) {
		this.nombreCiudad = nombreCiudad;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public MCiudad(Long idCiudad, String nombreCiudad, String descripcion) {
		
		this.idCiudad = idCiudad;
		this.nombreCiudad = nombreCiudad;
		this.descripcion = descripcion;
	}
	
	public MCiudad(Ciudad ciudad) {
		
		this.idCiudad = ciudad.getIdCiudad();
		this.nombreCiudad = ciudad.getNombreCiudad();
		this.descripcion = ciudad.getDescripcion();
	}
	
	
	
    
}

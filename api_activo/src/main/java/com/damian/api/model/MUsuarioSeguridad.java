package com.damian.api.model;

import com.damian.api.entity.UsuarioSeguridad;

public class MUsuarioSeguridad {
	
	 private Long idUsuarioSeguridad;
	 private String username;
	 private String password;
	 
	 
	public Long getIdUsuarioSeguridad() {
		return idUsuarioSeguridad;
	}
	public void setIdUsuarioSeguridad(Long idUsuarioSeguridad) {
		this.idUsuarioSeguridad = idUsuarioSeguridad;
	}
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public MUsuarioSeguridad(UsuarioSeguridad usuarioSeguridad) {
			
			this.idUsuarioSeguridad=usuarioSeguridad.getIdUsuarioSeguridad();
			this.username=usuarioSeguridad.getUsername();
			this.password=usuarioSeguridad.getPassword();
	}
	
	public MUsuarioSeguridad(Long idUsuarioSeguridad, String username, String password) {
		super();
		this.idUsuarioSeguridad = idUsuarioSeguridad;
		this.username = username;
		this.password = password;
	}
	
	
	 
	 
}

package com.damian.api.model;

import com.damian.api.entity.TipoActivo;

public class MTipoActivo {
	
	
		private Long idTipoActivo;
	    private String nombreTipoActivo;
	    private String descripcionTipoActivo;
	    
	    
		public Long getIdTipoActivo() {
			return idTipoActivo;
		}
		public void setIdTipoActivo(Long idTipoActivo) {
			this.idTipoActivo = idTipoActivo;
		}
		public String getNombreTipoActivo() {
			return nombreTipoActivo;
		}
		public void setNombreTipoActivo(String nombreTipoActivo) {
			this.nombreTipoActivo = nombreTipoActivo;
		}
		public String getDescripcionTipoActivo() {
			return descripcionTipoActivo;
		}
		public void setDescripcionTipoActivo(String descripcionTipoActivo) {
			this.descripcionTipoActivo = descripcionTipoActivo;
		}
		
		public MTipoActivo(Long idTipoActivo, String nombreTipoActivo, String descripcionTipoActivo) {
			super();
			this.idTipoActivo = idTipoActivo;
			this.nombreTipoActivo = nombreTipoActivo;
			this.descripcionTipoActivo = descripcionTipoActivo;
		}
		
		public MTipoActivo(TipoActivo tipoActivo) {
			
			this.idTipoActivo = tipoActivo.getIdTipoActivo();
			this.nombreTipoActivo = tipoActivo.getNombreTipoActivo();
			this.descripcionTipoActivo = tipoActivo.getDescripcionTipoActivo();
			
		}
		
		
	    
	    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.damian.api.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author Equipo
 */
@Entity
@Table(name = "area_activo")
@NamedQueries({
    @NamedQuery(name = "AreaActivo.findAll", query = "SELECT a FROM AreaActivo a")})
public class AreaActivo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
  	@SequenceGenerator(name="activosdb_id_area_activo_seq",sequenceName="activosdb_id_area_activo_seq",allocationSize=1)
  	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="activosdb_id_area_activo_seq")
    @Basic(optional = false)
    @Column(name = "id_area_activo")
    private Long idAreaActivo;
    @Column(name = "nombre_area_activo")
    private String nombreAreaActivo;
    @Column(name = "descripcion_area_activo")
    private String descripcionAreaActivo;
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JoinColumn(name = "id_ciudad", referencedColumnName = "id_ciudad")
    @ManyToOne(fetch = FetchType.LAZY)
    private Ciudad idCiudad;

    public AreaActivo() {
    }

    public AreaActivo(Long idAreaActivo) {
        this.idAreaActivo = idAreaActivo;
    }

    public AreaActivo(Long idAreaActivo, String nombreAreaActivo, String descripcionAreaActivo) {
        this.idAreaActivo = idAreaActivo;
        this.nombreAreaActivo = nombreAreaActivo;
        this.descripcionAreaActivo = descripcionAreaActivo;
    }

    public Long getIdAreaActivo() {
        return idAreaActivo;
    }

    public void setIdAreaActivo(Long idAreaActivo) {
        this.idAreaActivo = idAreaActivo;
    }

    public String getNombreAreaActivo() {
        return nombreAreaActivo;
    }

    public void setNombreAreaActivo(String nombreAreaActivo) {
        this.nombreAreaActivo = nombreAreaActivo;
    }

    public String getDescripcionAreaActivo() {
        return descripcionAreaActivo;
    }

    public void setDescripcionAreaActivo(String descripcionAreaActivo) {
        this.descripcionAreaActivo = descripcionAreaActivo;
    }


    public Ciudad getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Ciudad idCiudad) {
        this.idCiudad = idCiudad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAreaActivo != null ? idAreaActivo.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "javaapplication1.AreaActivo[ idAreaActivo=" + idAreaActivo + " ]";
    }

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	public AreaActivo(AreaActivo areaActivo) {
		super();
		this.idAreaActivo = areaActivo.getIdAreaActivo();
		this.nombreAreaActivo = areaActivo.getNombreAreaActivo();
		this.descripcionAreaActivo = areaActivo.getDescripcionAreaActivo();
		this.idCiudad = areaActivo.getIdCiudad();
	}
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.damian.api.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Equipo
 */
@Entity
@Table(name = "estado_activo")
@NamedQueries({
    @NamedQuery(name = "EstadoActivo.findAll", query = "SELECT e FROM EstadoActivo e")})
public class EstadoActivo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
   	@SequenceGenerator(name="activosdb_id_estado_activo_seq",sequenceName="activosdb_id_estado_activo_seq",allocationSize=1)
   	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="activosdb_id_estado_activo_seq")
    @Basic(optional = false)
    @Column(name = "id_estado_activo")
    private Long idEstadoActivo;
    @Column(name = "nombre_estado")
    private String nombreEstado;
    @Column(name = "descripcion_estado")
    private String descripcionEstado;

    public EstadoActivo() {
    }

    public EstadoActivo(Long idEstadoActivo) {
        this.idEstadoActivo = idEstadoActivo;
    }

    public EstadoActivo(Long idEstadoActivo, String nombreEstado, String descripcionEstado) {
        this.idEstadoActivo = idEstadoActivo;
        this.nombreEstado = nombreEstado;
        this.descripcionEstado = descripcionEstado;
    }

    public Long getIdEstadoActivo() {
        return idEstadoActivo;
    }

    public void setIdEstadoActivo(Long idEstadoActivo) {
        this.idEstadoActivo = idEstadoActivo;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }

    public String getDescripcionEstado() {
        return descripcionEstado;
    }

    public void setDescripcionEstado(String descripcionEstado) {
        this.descripcionEstado = descripcionEstado;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstadoActivo != null ? idEstadoActivo.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "javaapplication1.EstadoActivo[ idEstadoActivo=" + idEstadoActivo + " ]";
    }

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	public EstadoActivo(EstadoActivo estadoActivo) {
		super();
		this.idEstadoActivo = estadoActivo.getIdEstadoActivo();
		this.nombreEstado = estadoActivo.getNombreEstado();
		this.descripcionEstado = estadoActivo.getDescripcionEstado();
	}
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.damian.api.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Equipo
 */
@Entity
@Table(name = "ciudad")
@NamedQueries({
    @NamedQuery(name = "Ciudad.findAll", query = "SELECT c FROM Ciudad c")})
public class Ciudad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
  	@SequenceGenerator(name="activosdb_id_ciudad_seq",sequenceName="activosdb_id_ciudad_seq",allocationSize=1)
  	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="activosdb_id_ciudad_seq")
    @Basic(optional = false)
    @Column(name = "id_ciudad")
    private Long idCiudad;
    @Column(name = "nombre_ciudad")
    private String nombreCiudad;
    @Column(name = "descripcion")
    private String descripcion;

    public Ciudad() {
    }

    public Ciudad(Long idCiudad) {
        this.idCiudad = idCiudad;
    }

    public Ciudad(Long idCiudad, String nombreCiudad, String descripcion) {
        this.idCiudad = idCiudad;
        this.nombreCiudad = nombreCiudad;
        this.descripcion = descripcion;
    }

    public Long getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Long idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getNombreCiudad() {
        return nombreCiudad;
    }

    public void setNombreCiudad(String nombreCiudad) {
        this.nombreCiudad = nombreCiudad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCiudad != null ? idCiudad.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "javaapplication1.Ciudad[ idCiudad=" + idCiudad + " ]";
    }

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	public Ciudad(Ciudad ciudad) {
		this.idCiudad = ciudad.getIdCiudad();
        this.nombreCiudad = ciudad.getNombreCiudad();
        this.descripcion = ciudad.getDescripcion();
	}
    
    
    
}

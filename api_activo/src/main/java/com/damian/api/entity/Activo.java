/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.damian.api.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Equipo
 */
@Entity
@Table(name = "activo")
@NamedQueries({
    @NamedQuery(name = "Activo.findAll", query = "SELECT a FROM Activo a")})
public class Activo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@SequenceGenerator(name="activosdb_id_activo_seq",sequenceName="activosdb_id_activo_seq",allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="activosdb_id_activo_seq")
    @Basic(optional = false)
    @Column(name = "id_activo")
    private Long idActivo;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "serial")
    private String serial;
    @Column(name = "numero_interno_inventario")
    private int numeroInternoInventario;
    @Column(name = "peso")
    private BigInteger peso;
    @Column(name = "alto")
    private BigInteger alto;
    @Column(name = "ancho")
    private BigInteger ancho;
    @Column(name = "largo")
    private BigInteger largo;
    @Basic(optional = false)
    @Column(name = "valor_compra")
    private double valorCompra;
    @Column(name = "fecha_compra")
    @Temporal(TemporalType.DATE)
    private Date fechaCompra;
    @Column(name = "fecha_baja")
    @Temporal(TemporalType.DATE)
    private Date fechaBaja;
    @Column(name = "color")
    private String color;
    @Column(name = "marca")
    private String marca;
    @Column(name = "cantidad")
    private BigInteger cantidad;
    @JoinColumn(name = "id_area_activo", referencedColumnName = "id_area_activo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AreaActivo idAreaActivo;
    @JoinColumn(name = "id_estado_activo", referencedColumnName = "id_estado_activo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private EstadoActivo idEstadoActivo;
    @JoinColumn(name = "id_tipo_activo", referencedColumnName = "id_tipo_activo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TipoActivo idTipoActivo;

    public Activo() {
    }

    public Activo(Long idActivo) {
        this.idActivo = idActivo;
    }

    public Activo(Long idActivo, String nombre, String descripcion, String serial, int numeroInternoInventario, BigInteger peso, double valorCompra, Date fechaCompra, String marca,BigInteger cantidad ) {
        this.idActivo = idActivo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.serial = serial;
        this.numeroInternoInventario = numeroInternoInventario;
        this.peso = peso;
        this.valorCompra = valorCompra;
        this.fechaCompra = fechaCompra;
        this.marca=marca;
        this.cantidad=cantidad;
    }

    public Long getIdActivo() {
        return idActivo;
    }

    public void setIdActivo(Long idActivo) {
        this.idActivo = idActivo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public int getNumeroInternoInventario() {
        return numeroInternoInventario;
    }

    public void setNumeroInternoInventario(int numeroInternoInventario) {
        this.numeroInternoInventario = numeroInternoInventario;
    }

    public BigInteger getPeso() {
        return peso;
    }

    public void setPeso(BigInteger peso) {
        this.peso = peso;
    }

    public BigInteger getAlto() {
        return alto;
    }

    public void setAlto(BigInteger alto) {
        this.alto = alto;
    }

    public BigInteger getAncho() {
        return ancho;
    }

    public void setAncho(BigInteger ancho) {
        this.ancho = ancho;
    }

    public BigInteger getLargo() {
        return largo;
    }

    public void setLargo(BigInteger largo) {
        this.largo = largo;
    }

    public double getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(double valorCompra) {
        this.valorCompra = valorCompra;
    }

    public Date getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public AreaActivo getIdAreaActivo() {
        return idAreaActivo;
    }

    public void setIdAreaActivo(AreaActivo idAreaActivo) {
        this.idAreaActivo = idAreaActivo;
    }

    public EstadoActivo getIdEstadoActivo() {
        return idEstadoActivo;
    }

    public void setIdEstadoActivo(EstadoActivo idEstadoActivo) {
        this.idEstadoActivo = idEstadoActivo;
    }

    public TipoActivo getIdTipoActivo() {
        return idTipoActivo;
    }

    public void setIdTipoActivo(TipoActivo idTipoActivo) {
        this.idTipoActivo = idTipoActivo;
    }
    
    public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public BigInteger getCantidad() {
		return cantidad;
	}

	public void setCantidad(BigInteger cantidad) {
		this.cantidad = cantidad;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idActivo != null ? idActivo.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "javaapplication1.Activo[ idActivo=" + idActivo + " ]";
    }

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}
    
    
}

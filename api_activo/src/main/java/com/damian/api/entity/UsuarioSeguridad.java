/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.damian.api.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Equipo
 */
@Entity
@Table(schema="public",name = "usuario_seguridad")
@NamedQueries({
    @NamedQuery(name = "UsuarioSeguridad.findAll", query = "SELECT u FROM UsuarioSeguridad u")})
public class UsuarioSeguridad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
   	@SequenceGenerator(name="activosdb_id_usuario_seguridad_seq",sequenceName="activosdb_id_usuario_seguridad_seq",allocationSize=1)
   	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="activosdb_id_usuario_seguridad_seq")
    @Basic(optional = false)
    @Column(name = "id_usuario_seguridad")
    private Long idUsuarioSeguridad;
    @Column(name="username")
	private String username;
    @Column(name = "password")
    private String password;

    public UsuarioSeguridad() {
    }

    public UsuarioSeguridad(Long idUsuarioSeguridad) {
        this.idUsuarioSeguridad = idUsuarioSeguridad;
    }

    public UsuarioSeguridad(Long idUsuarioSeguridad, String username, String password) {
        this.idUsuarioSeguridad = idUsuarioSeguridad;
        this.username = username;
        this.password = password;
    }

    public Long getIdUsuarioSeguridad() {
        return idUsuarioSeguridad;
    }

    public void setIdUsuarioSeguridad(Long idUsuarioSeguridad) {
        this.idUsuarioSeguridad = idUsuarioSeguridad;
    }


    
    public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuarioSeguridad != null ? idUsuarioSeguridad.hashCode() : 0);
        return hash;
    }

    
    @Override
    public String toString() {
        return "javaapplication1.UsuarioSeguridad[ idUsuarioSeguridad=" + idUsuarioSeguridad + " ]";
    }

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.damian.api.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Equipo
 */
@Entity
@Table(name = "tipo_activo")
@NamedQueries({
    @NamedQuery(name = "TipoActivo.findAll", query = "SELECT t FROM TipoActivo t")})
public class TipoActivo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
   	@SequenceGenerator(name="activosdb_id_tipo_activo_seq",sequenceName="activosdb_id_tipo_activo_seq",allocationSize=1)
   	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="activosdb_id_tipo_activo_seq")
    @Basic(optional = false)
    @Column(name = "id_tipo_activo")
    private Long idTipoActivo;
    @Column(name = "nombre_tipo_activo")
    private String nombreTipoActivo;
    @Column(name = "descripcion_tipo_activo")
    private String descripcionTipoActivo;

    public TipoActivo() {
    }

    public TipoActivo(Long idTipoActivo) {
        this.idTipoActivo = idTipoActivo;
    }

    public TipoActivo(Long idTipoActivo, String nombreTipoActivo) {
        this.idTipoActivo = idTipoActivo;
        this.nombreTipoActivo = nombreTipoActivo;
    }

    public Long getIdTipoActivo() {
        return idTipoActivo;
    }

    public void setIdTipoActivo(Long idTipoActivo) {
        this.idTipoActivo = idTipoActivo;
    }

    public String getNombreTipoActivo() {
        return nombreTipoActivo;
    }

    public void setNombreTipoActivo(String nombreTipoActivo) {
        this.nombreTipoActivo = nombreTipoActivo;
    }

    public String getDescripcionTipoActivo() {
        return descripcionTipoActivo;
    }

    public void setDescripcionTipoActivo(String descripcionTipoActivo) {
        this.descripcionTipoActivo = descripcionTipoActivo;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoActivo != null ? idTipoActivo.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "javaapplication1.TipoActivo[ idTipoActivo=" + idTipoActivo + " ]";
    }

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	public TipoActivo(TipoActivo tipoActivo) {
		super();
		this.idTipoActivo = tipoActivo.getIdTipoActivo();
		this.nombreTipoActivo = tipoActivo.getNombreTipoActivo();
		this.descripcionTipoActivo = tipoActivo.getDescripcionTipoActivo();
	}
    
    
    
}

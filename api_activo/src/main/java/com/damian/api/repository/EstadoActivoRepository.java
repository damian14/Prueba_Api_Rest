package com.damian.api.repository;

import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.damian.api.entity.EstadoActivo;


@Repository("repositoryEstadoActivity")
public interface EstadoActivoRepository extends JpaRepository<EstadoActivo, Serializable>{
	
	public abstract Page<EstadoActivo> findAll(Pageable pageable);
	
	public abstract EstadoActivo findByIdEstadoActivo(Long idEstadoActivo);

}

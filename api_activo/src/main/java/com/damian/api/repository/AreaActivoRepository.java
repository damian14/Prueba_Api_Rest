package com.damian.api.repository;

import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.damian.api.entity.AreaActivo;

@Repository("repositoryAreaActivo")
public interface AreaActivoRepository extends JpaRepository<AreaActivo, Serializable> {

	public abstract Page<AreaActivo> findAll(Pageable pageable);
	
	public abstract AreaActivo findByIdAreaActivo(Long idAreaActivo);

}

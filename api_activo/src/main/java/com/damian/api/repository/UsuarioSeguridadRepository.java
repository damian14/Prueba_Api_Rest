package com.damian.api.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.damian.api.entity.UsuarioSeguridad;

@Repository("repositoryUsuarioSeguridad")
public interface UsuarioSeguridadRepository  extends JpaRepository<UsuarioSeguridad, Serializable>{
	
	public abstract UsuarioSeguridad findByUsername(String username);

}

package com.damian.api.repository;

import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.damian.api.entity.TipoActivo;

@Repository("repositoryTipoActivo")
public interface TipoActivoRepository extends JpaRepository<TipoActivo, Serializable> {
	
	public abstract Page<TipoActivo> findAll(Pageable pageable);
	
	public abstract TipoActivo findByIdTipoActivo(Long idTipoActivo);

}

package com.damian.api.repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.damian.api.entity.Activo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Repository("repositoryActivo")
public interface ActivoRepository extends JpaRepository<Activo, Serializable>{ 
	
	public abstract Page<Activo> findAll(Pageable pageable);
	
	public List<Activo> findByIdTipoActivoIdTipoActivoAndFechaCompraAndSerial(Long idTipoActivo , Date fechaCompra , String serial);
	
	public List<Activo> findByIdTipoActivoIdTipoActivoAndSerial(Long idTipoActivo,String serial);
	@Modifying
	@Transactional
	@Query("update Activo a set a.serial = :serial , a.fechaBaja = :fechaBaja where a.idActivo = :idActivo")
	public void actualizarActivo(@Param("idActivo")Long idActivo, @Param("serial")String serial , @Param("fechaBaja") Date fechaBaja);
	
	public Activo findByIdActivo(Long idActivo);
	
	public Activo findBySerial(String serial);
	
	
	
}

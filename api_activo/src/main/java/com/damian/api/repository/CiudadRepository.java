package com.damian.api.repository;

import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.damian.api.entity.Ciudad;

@Repository("repositoryCiudad")
public interface CiudadRepository extends JpaRepository<Ciudad, Serializable> {
	
	public abstract Page<Ciudad> findAll(Pageable pageable);
	
	public abstract Ciudad findByIdCiudad(Long idCiudad);

}
